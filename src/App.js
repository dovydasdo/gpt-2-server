import "./App.css";
import React, { useState, useContext, useEffect } from "react";
import axios from "axios";
import {
  Button,
  Container,
  Row,
  Col,
  InputGroup,
  FormControl,
  Dropdown
} from "react-bootstrap";
import { ControlPanel } from "./components/controlPanel.js";
import Store from "./store.js";
import { Context } from "./store.js";

export default function AppWrapper() {
  return (
    <Store>
      <App />
    </Store>
  );
}

function App() {

  
  const [getText, setGetText] = useState({});
  const [prompt, setPrompt] = useState("");
  const [loading, setLoading] = useState(false);
  const [params, setParams] = useState({});
  const [state, dispatch] = useContext(Context);
  const [model, setModel] = useState("fqa");


  useEffect(() => {
    console.log(state);
    setParams({
      temperature: state.temperature,
      length: state.length,
      topK: state.topK,
      topP: state.topP,
      nGram: state.nGram,
      repPen: state.repPen,
      lenPen: state.lenPen
    });
  }, []);

  const handleModelSelect= e =>{
    console.log(e)
    setModel(e)
  }


  const getTextAPI = () => {
    if(prompt === ""){
      alert("Iveskite pradini teksta")
      return
    }
    setLoading(true);
    console.log(model)
    axios
      .get("http://127.0.0.1:5000/generate", {
        params: {
          prompt: prompt,
          temperature: state.temperature,
          length: state.length,
          topK: state.topK,
          topP: state.topP,
          nGram: state.nGram,
          repPen: state.repPen,
          lenPen: state.lenPen, 
          model: model
        }
      })
      .then(response => {
        console.log("SUCCESS", response);
        setGetText(response);
        setLoading(false);
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <Container>
      <Row>
        <header>
          <h1> GPT-2 Generator</h1>
          <h2>Selected model: {model === "lit-comms"? "Lithuanian comments" :""} {model === "fqa"? "Financial Q&A" :""}{model === "fgen"? "Financial comment generation" :""}</h2>
        </header>
      </Row>
      <Row>
        <ControlPanel></ControlPanel>
        <Dropdown onSelect={handleModelSelect}>
          <Dropdown.Toggle id="dropdown-basic-button">
            Select model
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item eventKey="lit-comms">Lithuanian comments</Dropdown.Item>
            <Dropdown.Item eventKey="fqa">Financial Q&A</Dropdown.Item>
            <Dropdown.Item eventKey="fgen">Financial comment generation</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Row>
      <Row>
        <Col md="5">
          <InputGroup>
            <InputGroup.Prepend>
              <InputGroup.Text>Prompt</InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl
              onChange={e => {
                setPrompt(e.target.value);
              }}
              as="textarea"
              aria-label="With textarea"
            />
          </InputGroup>
        </Col>
        <Col md="auto">
          <Button type="primary" onClick={getTextAPI} active={!loading}>
            Generate
          </Button>
        </Col>
      </Row>
      <Row>
        <div>
          {loading ? (
            "Generating..."
          ) : getText.status === 200 ? (
            <h3>{getText.data.message}</h3>
          ) : (
            ""
          )}
        </div>
      </Row>
    </Container>
  );
}
