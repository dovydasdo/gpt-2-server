const Reducer = (state, action) => {
  switch (action.type) {
    case "SET_TEMP":
      return {
        ...state,
        temperature: action.payload
      };
    case "SET_MAX_LEN":
      return {
        ...state,
        length: action.payload
      };
    case "SET_TOP_K":
      return {
        ...state,
        topK: action.payload
      };
    case "SET_TOP_P":
      return {
        ...state,
        topP: action.payload
      };
    case "SET_NGRAM":
      return {
        ...state,
        nGram: action.payload
      };
    case "SET_REP_PEN":
      return {
        ...state,
        repPen: action.payload
      };
    case "SET_LEN_PEN":
      return {
        ...state,
        lenPen: action.payload
      };
    case "SET_SAMPLING":
      return {
        ...state,
        sampling: action.payload
      };
    default:
      return state;
  }
};

export default Reducer;