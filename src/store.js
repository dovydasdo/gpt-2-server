import React, {createContext, useReducer} from "react";
import Reducer from './reducer.js'


const initialState = {
    temperature: 1,
    length: 50,
    topK: 50,
    topP: 1,
    nGram: 12,
    repPen: 1,
    lenPen: 1,
    sampling: true,
};

const Store = ({children}) => {
    const [state, dispatch] = useReducer(Reducer, initialState);
    return (
        <Context.Provider value={[state, dispatch]}>
            {children}
        </Context.Provider>
    )
};

export const Context = createContext(initialState);
export default Store;