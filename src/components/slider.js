import React, { useState, useEffect, useContext } from "react";
import { Row, Col, Form } from "react-bootstrap";
import RangeSlider from "react-bootstrap-range-slider";
import { Context } from "../store.js";

export const Slider = props => {
  const [value, setValue] = useState(Math.floor((props.min + props.max) / 2));
  const [state, dispatch] = useContext(Context);

  useEffect(() => {
    switch (props.type) {
      case "temp":
        dispatch({ type: "SET_TEMP", payload: value });
        break;
      case "max_length":
        dispatch({ type: "SET_MAX_LEN", payload: value });
        break;
      case "top_p":
        dispatch({ type: "SET_TOP_P", payload: value });
        break;
      case "top_k":
        dispatch({ type: "SET_TOP_K", payload: value });
        break;
      case "n_gram":
        dispatch({ type: "SET_NGRAM", payload: value });
        break;
      case "rep_pen":
        dispatch({ type: "SET_REP_PEN", payload: value });
        break;
      case "len_pen":
        dispatch({ type: "SET_LEN_PEN", payload: value });
        break;
      default:
        break;
    }
  }, [dispatch, props.type, value]);

  return (
    <div>
      <Form>
        <Form.Group as={Row}>
          <Col xs="9">
            <RangeSlider
              value={value}
              onChange={e =>{ setValue(e.target.value)}}
              min={props.min}
              max={props.max}
              step={props.step}
            />
          </Col>
          <Col xs="3">
            <Form.Control value={value} />
          </Col>
        </Form.Group>
      </Form>
    </div>
  );
};
