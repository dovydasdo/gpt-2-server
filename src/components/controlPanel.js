import { Row } from "react-bootstrap";
import { Slider } from "./slider.js";

export const ControlPanel = props => {

  return (
    <div>
      <Row md="12">
        <Slider min={0.1} max={2} step={0.1} type="temp"></Slider>
        <h2 Style={"margin-left: 15px"}>Temperature</h2>
      </Row>
      <Row md="12">
        <Slider min={10} max={200} step={1} type="max_length"></Slider>
        <h2 Style={"margin-left: 15px"}>Max length</h2>
      </Row>
      <Row md="12">
        <Slider min={0.1} max={2} step={0.1} type="top_p"></Slider>
        <h2 Style={"margin-left: 15px"}>Top P</h2>
      </Row>
      <Row md="12">
        <Slider min={5} max={100} step={1} type="top_k"></Slider>
        <h2 Style={"margin-left: 15px"}>Top K</h2>
      </Row>
      <Row md="12">
        <Slider min={1} max={40} step={1} type="n_gram"></Slider>
        <h2 Style={"margin-left: 15px"}>No repeat n-gram size</h2>
      </Row>
      <Row md="12">
        <Slider min={0.1} max={2} step={0.1} type="rep_pen"></Slider>
        <h2 Style={"margin-left: 15px"}>Repetition penalty</h2>
      </Row>
      <Row md="12">
        <Slider min={0.1} max={2} step={0.1} type="len_pen"></Slider>
        <h2 Style={"margin-left: 15px"}>Length penalty</h2>
      </Row>
    </div>
  );
};
