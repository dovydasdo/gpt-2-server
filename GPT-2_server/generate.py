from transformers import GPT2Tokenizer
from transformers import AutoModelWithLMHead


class Generator():

    def __init__(self, model):
        if model == "lit-comms":
            self.model = AutoModelWithLMHead.from_pretrained("GPT-2_model/lit-comms")
        if model == "fqa":
            self.model = AutoModelWithLMHead.from_pretrained("./GPT-2_model/fqa")
        if model == "fgen":
            self.model = AutoModelWithLMHead.from_pretrained("./GPT-2_model/nq_model")
        self.tokenizer = GPT2Tokenizer.from_pretrained('gpt2')

    def get_text(self, prompt, temperature, length, topK, topP, nGram, repPen, lenPen, model):

        if model == "fqa":
            prompt = prompt + " <EOQ>"

        inputs = self.tokenizer.encode(prompt, add_special_tokens=False, return_tensors="pt")
        prompt_length = len(
            self.tokenizer.decode(inputs[0], skip_special_tokens=True, clean_up_tokenization_spaces=True))

        outputs = self.model.generate(inputs,
                                      max_length=length,
                                      do_sample=True,
                                      top_p=topP,
                                      top_k=topK,
                                      output_attentions=False,
                                      length_penalty=lenPen,
                                      temperature=temperature,
                                      no_repeat_ngram_size=nGram,
                                      repetition_penalty=lenPen)

        if model == "fqa":
            out = self.tokenizer.decode(outputs[0])
            generated = out.split("<EOS>")[0]
            generated = generated.split("<EOQ>")[-1]
        else:
            generated = prompt + self.tokenizer.decode(outputs[0])[prompt_length:]

        return generated
