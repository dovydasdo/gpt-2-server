from flask import Flask, render_template, request
from generate import Generator
from flask_cors import CORS
from flask_restful import Api, Resource, reqparse

app = Flask(__name__)
CORS(app)

api = Api(app)

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/generate', methods=['GET'])
def generate():
    gen = Generator(request.args["model"])
    text = gen.get_text(request.args['prompt'],
                        float(request.args["temperature"]),
                        int(request.args["length"]),
                        int(request.args["topK"]),
                        float(request.args["topP"]),
                        int(request.args["nGram"]),
                        float(request.args["repPen"]),
                        float(request.args["lenPen"]),
                        str(request.args["model"]))
    final_ret = {"status": "Success", "message": text}

    return final_ret


